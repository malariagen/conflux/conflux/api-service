ARG CONTRACT_VERSION="v0.2.6"
ARG CONTRACT_REGISTRY="registry.gitlab.com/malariagen/conflux/api-contract"

FROM python:3.8.3-alpine3.12 as builder

RUN apk add gcc postgresql-dev libpq musl-dev

RUN pip install psycopg2==2.8.5

FROM $CONTRACT_REGISTRY:$CONTRACT_VERSION AS contract

#--------------------------------------------
FROM python:3.8.3-alpine3.12

ARG PORT=5000
ARG SCRIPTS_PATH="production/"
EXPOSE $PORT

WORKDIR /app

RUN apk add libpq

COPY config/requirements.txt /tmp
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt

COPY config/$SCRIPTS_PATH/requirements.txt /tmp/extra_requirements.txt
RUN pip3 install --no-cache-dir -r /tmp/extra_requirements.txt

COPY --from=builder /usr/local/lib/python3.8/site-packages/psycopg2 \
/usr/local/lib/python3.8/site-packages/psycopg2/

COPY --from=builder /usr/local/lib/python3.8/site-packages/psycopg2-2.8.5.dist-info \
/usr/local/lib/python3.8/site-packages/psycopg2-2.8.5.dist-info/

COPY config/$SCRIPTS_PATH/start /start
RUN sed -i "s/PORT/$PORT/" /start
RUN chmod +x /start

COPY --from=contract /app/contract.yaml /contract/api.yaml

COPY ./conflux_server /app/conflux_server/

ENTRYPOINT ["sh","-c"]
CMD ["/start"]
