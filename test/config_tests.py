import sys
import os
import unittest

sys.path.append("..")

from conflux_server.env_config import *

class config_tests(unittest.TestCase):
#-----------------------------------------------------------------------
 def setUp(self):
  os.environ["DATABASE_URL"] = ""
  os.environ["POSTGRES_USER"] = ""
  os.environ["POSTGRES_DB"] = ""
  os.environ["POSTGRES_PASSWORD"] = ""
  os.environ["POSTGRES_HOST"] = ""
  os.environ["POSTGRES_PORT"] = ""

#-----------------------------------------------------------------------
 def test_database_url_parser_format(self):
  result = parse_database_url("postgres://user:password@postgres-host:1234/postgres-database")
  if not result[0]:
   print("Error: {}".format(result[1]))

  self.assertTrue(result[0])
  self.assertEqual(result[1]["user"],"user")
  self.assertEqual(result[1]["password"],"password")
  self.assertEqual(result[1]["host"],"postgres-host")
  self.assertEqual(result[1]["port"],"1234")
  self.assertEqual(result[1]["database"],"postgres-database")

#-----------------------------------------------------------------------
 def test_database_url_parser_real(self):
  result = parse_database_url("postgres://jk23:jP5Z#=a6z{b;k8=(@example.internal.sanger.ac.uk:5432/api-service-db")
  if not result[0]:
   print("Error: {}".format(result[1]))

  self.assertTrue(result[0])
  self.assertEqual(result[1]["user"],"jk23")
  self.assertEqual(result[1]["password"],"jP5Z#=a6z{b;k8=(")
  self.assertEqual(result[1]["host"],"example.internal.sanger.ac.uk")
  self.assertEqual(result[1]["port"],"5432")
  self.assertEqual(result[1]["database"],"api-service-db")

#-----------------------------------------------------------------------
 def test_get_connection_with_db_url(self):
  os.environ["POSTGRES_USER"] = "jk32"
  os.environ["POSTGRES_DB"] = "database"
  os.environ["POSTGRES_PASSWORD"] = "password123"
  os.environ["POSTGRES_HOST"] = "localhost"
  os.environ["POSTGRES_PORT"] = "5433"
  os.environ["DATABASE_URL"] = "postgres://jk23:jP5Z#=a6z{b;k8=(@example.internal.sanger.ac.uk:5432/api-service-db"

  result = get_connection_config()
  self.assertEqual(result["user"],"jk23")
  self.assertEqual(result["password"],"jP5Z#=a6z{b;k8=(")
  self.assertEqual(result["host"],"example.internal.sanger.ac.uk")
  self.assertEqual(result["port"],"5432")
  self.assertEqual(result["database"],"api-service-db")

#-----------------------------------------------------------------------
 def test_get_connection_without_db_url(self):
  os.environ["POSTGRES_USER"] = "jk32"
  os.environ["POSTGRES_DB"] = "database"
  os.environ["POSTGRES_PASSWORD"] = "password123"
  os.environ["POSTGRES_HOST"] = "localhost"
  os.environ["POSTGRES_PORT"] = "5433"

  result = get_connection_config()
  self.assertEqual(result["user"],"jk32")
  self.assertEqual(result["password"],"password123")
  self.assertEqual(result["host"],"localhost")
  self.assertEqual(result["port"],"5433")
  self.assertEqual(result["database"],"database")

#-----------------------------------------------------------------------
#-----------------------------------------------------------------------

if __name__ == '__main__':
 unittest.main()
