# api-service

MalariaGEN Conflux api service.

## API Contract

The API Service has a dependency on the OpenAPI contract from the api-contract
repository. This used to be added using a `git submodule` but this is now
imported from the docker image that is built by the api-contract repository. 

This import happens in the `Dockerfile` for this project. A new stage has
been added that imports the `registry.gitlab.com/malariagen/conflux/api-contract`
docker image, which can be targeted to a specific version using the `CONTRACT_VERSION`
build arg. The `/app/contract.yml` is then copied from that image into the 
`/contract` folder in the docker image from the API Service. 

### Local Development

Locally, you can specify an alternative CONTRACT_VERSION, by creating a `.env`
file and adding 

    CONTRACT_VERSION=<some_version>
    CONTRACT_REGISTRY=registry.gitlab.com/malariagen/conflux/api-contract

Or copy the `.env.example` to `.env` and edit it.

You will then need to rebuild the docker image by running `docker-compose -f local.yml build`
or `docker-compose -f local.yml up --build`.

If you need to use a specific version of the contract from a branch, rather
than a tagged version, you can specify the `CONTRACT_REGISTRY` build arg
by altering the same in the `.env` and point to new registry path. For more
information about possible values, see the registry for the `api-contract` 
repository.

https://gitlab.com/malariagen/conflux/api-contract/container_registry

### Upgrading Default Version

When you need to upgrade the default version that the project uses, you 
should edit that in two places. 

**`.env.example`**

You should change the environment variable called `CONTRACT_VERSION` to be
the version that the api-service should now track.

    CONTRACT_VERSION=test

**`Dockerfile`**

You should change the default for the build ARG that is defined in the 
Dockerfile here as well. 

    ARG CONTRACT_VERSION="latest"

### Gitlab CI/CD

By default the Gitlab CI/CD will build the application using the default 
values for the docker build ARGS as defined in the Dockerfile. The values
for CONTRACT_VERSION and CONTRACT_REGISTRY should be changed in the repository
so that when building an older version of the api-service, it will use
the version of the api-contract that the service supports. 

However, if it becomes necessary to define the CONTRACT_VERSION in the Gitlab
CI, this can be achieved by adding the following to the main `variables` section
in the `.gitlab-ci.yml` file. 

    variables:
      ...
      AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS: --build-arg=CONTRACT_VERSION=$CONTRACT_VERSION
      
You can now change the `CONTRACT_VERSION` using the Gitlab CI/CD variables, by 
either defining `CONTRACT_VERSION` in the `.gitlab-ci.yml` file  

    variables:
      ...
      AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS: --build-arg=CONTRACT_VERSION=$CONTRACT_VERSION
      CONTRACT_VERSION=v0.1.1
      
Or you can choose to define the `CONTRACT_VERSION` in the Gitlab CI/CD 
variables in the settings for the repository on Gitlab.com
  

## Before running

A s3 bucket containing the output from the extractors [mlwh](https://gitlab.com/malariagen/conflux/extractors/mlwh) and [roma](https://gitlab.com/malariagen/conflux/extractors/roma) to run. The details for this bucket are provided via the environmental variables `SDS_BUCKET`, `SDS_ACCESS`, and `SDS_SECRET`.

Before running you will need to edit `local.yml` to include the values.

## Local Docker environment

To run the local docker environment

    `docker-compose -f local.yml up -d`

Your site will be available at `localhost:8050`

To review the logs for the docker environment

    `docker-compose -f local.yml logs -f`

This will output all the logs from the two docker services and update as new
logs are added.

If you edit the dockerfile remember to include `--build` in the docker-compose command. E.g   `docker-compose -f local.yml up --build`

## Container security scanning with Clair

You can test your locally built docker images with clair using the `bin/clair`
command line script.

To start the clair server run `bin/clair start`

To test a local docker image run `bin/clair test [IMAGE]`. If the image is not local
please pull the image using `docker pull [IMAGE]` or build the image locally.

To stop the clair server run `bin/clair stop`

This setup runs on a linux machine. If you are running this on another OS, you will
probably need help setting that up. Please contact matt@outlandish.com for support.

Information on the software used can be found here:
https://github.com/quay/clair
https://github.com/arminc/clair-scanner
