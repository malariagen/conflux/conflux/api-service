"""
Module to hold schedular based classes and methods.
Uses apscheduler for actual scheduling
"""

import sys

from apscheduler.schedulers.background import BackgroundScheduler

class DecoratedScheduler(BackgroundScheduler):
 """ Wrapper for apscheduler to provide a startup method and decorator.
 I learnt too late that there is already a decorator for this!
 But I like that it provides startup and schedule register in one
 so I will keep it for now """
 def __init__(self):
  """ init setups up run_on_start and super BackgroundScheduler """
  self.run_on_start = []
  self.has_started = False
  BackgroundScheduler.__init__(self)
 #------------------------------------------------------------------------------
 def schedule(self, run_on_start=False, minutes=0, hours=0, seconds=0):
  """ decorator to schedule a function with the option to add it to the startup
  list """
  def decorator(func):
   self.add_job(
       func,
       "interval",
       minutes=minutes,
       hours=hours,
       seconds=seconds,
       replace_existing=True
   )
   if run_on_start:
    self.run_on_start.append(func)
   return func
  return decorator

 #------------------------------------------------------------------------------
 def startup(self):
  """ startup. Should only be called once. starts BackgroundScheduler if not
  already running and calls all funcions added to run_on_start if has_started
  is set to False """
  if not self.running:
   self.start()

  # Check is for debug mode that restarts the service on change.
  # I would prefer to check is cache exists and run if not per function
  # but flask-caching does not seem to support this.
  if not self.has_started:
   self.has_started = True
   for func in self.run_on_start:
    func()
