
# -------------------------------------------------------------
def get_studies(**kargs):
 return {"studies": [],"_links":[]}


# -------------------------------------------------------------
def get_format_metadata(**kargs):
 """
 {
   "fields": [
     {
       "name": "ids.study_id",
       "title": "Study ID",
       "type": "ID<Study>"
     },
     {
       "name": "events.name=malgen_lab.0",
       "title": "MalGen Lab",
       "type": "Statuses"
     }
   ]
 }
 """
 return {"fields":[]}
