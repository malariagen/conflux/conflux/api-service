
from flask import request, render_template, jsonify

from conflux_server import app, cache, scheduler
from conflux_server import simple_data_store

# ------------------------------------------------------------------------------
def get_samples(**kargs):
 #X-Request-ID <- header

 logger = app.app.logger
 logger.info("called get_samples")

 offset = 0
 if "offset" in kargs:
  offset = int(kargs["offset"])

 limit = 0
 if "limit" in kargs:
  limit = int(kargs["limit"])

 request_id = request.headers.get("X-Request-ID","")

 event_totals = simple_data_store.generate_sample_summary()

 samples,sample_count = simple_data_store.get_samples_page(offset,limit)

 return {
     "meta":{
         "limit": limit,
         "offset": offset,
         "request_id": request_id,
         "summary": {
             "events": event_totals
             },
         "total_results":sample_count,
         "total_filtered_results":sample_count
         },
     "rows": samples
 }

# ------------------------------------------------------------------------------
def get_format_metadata(**kargs):

 return {
     "fields": [
    {
      "name": "ids.sample_id",
      "title": "Sample ID",
      "type": "ID<Sample>"
    },
    {
      "name": "events.name=malgen_lab.0",
      "title": "MalGen Lab",
      "type": "Event"
    },
    {
      "name": "events.name=malgen_lab.0",
      "title": "MalGen Lab",
      "type": "Event"
    },
    {
      "name": "events.name=sanger_core.0",
      "title": "Sanger Core",
      "type": "Event"
    },
    {
      "name": "events.name=gbs_core.0",
      "title": "Sanger Core GBS",
      "type": "Event"
    }
  ]
}
