"""
Starts the server if conflux-server is imported as a module
"""

import sys

from conflux_server import get_app

if __name__ == "__main__":
 PORT = 5000
 CONTRACT_PATH = "/contract"
 if len(sys.argv) > 1:
  PORT = int(sys.argv[1])
 if len(sys.argv) > 2:
  CONTRACT_PATH = int(sys.argv[2])

 my_app, db, app_reload = get_app(PORT, CONTRACT_PATH)
 my_app.run(port=PORT, use_reloader=app_reload)
