"""
Simple datastore interface
"""

from time import time
from flask import Blueprint
from flask import g

from conflux_server import app, cache, scheduler
from conflux_server import env_config
from conflux_server.simple_data_store import s3_tools
from conflux_server import decorators

#-------------------------------------------------------------------------------
def get_events():
 return [
     "malgen_lab",
     "sanger_core",
     "gbs_core",
     "gbs_analysis",
     "wgs_core",
     "wgs_analysis"
 ]

#-------------------------------------------------------------------------------
def get_event_status(event,start,end):
 status = "N/A"
 if start is None:
  start = "-"
  end = "-"
 elif end is None:
  end = "-"
  status = "pending"
 else:
  status = "complete"


 return {
     "name":event,
     "done":end,
     "started":start,
     "status":status
 }

#-------------------------------------------------------------------------------
def guess_species_by_origin(origin):
 guess = {
     "spotmalaria":"pf",
     "vivax":"pv",
     "vobs":"ag",
     "genre":"pf"
 }

 if origin in guess:
  return guess[origin]

 return "Unknown"

#-- Sample methods -------------------------------------------------------------
@cache.cached(key_prefix='sample_cache')
def get_samples():
 """ cached function to load sample data from ROMA SDS and MLWH SDS and stitch
 them together """
 stamp = time()
 app.app.logger.info("running get_samples {}".format(stamp))

 #Create connection
 connection_details = env_config.get_sds_connection()
 bucket = connection_details["bucket"]#"airflow.tests"
 s3_client = s3_tools.get_client(
     connection_details["access"],
     connection_details["secret"]
 )

 key_names = s3_tools.get_object_keys(s3_client, bucket)
 mlwh_key = s3_tools.select_latest_key_from_list(key_names, "mlwh_samples_")
 roma_key = s3_tools.select_latest_key_from_list(key_names, "roma_model_samples.sample_")
 roma_studies_key = s3_tools.select_latest_key_from_list(key_names, "managements.study_")

 #Need to link in studies

 if mlwh_key is None:
  return {"error":"No MLWH samples found"}

 if roma_key is None:
  return {"error":"No Roma samples found"}

 mlwh_samples = s3_tools.get_tsv_from_object(s3_client, bucket, mlwh_key)
 roma_samples = s3_tools.get_json_from_object(s3_client, bucket, roma_key)

 samples = []
 mlwh_index = {v[1]:i for i, v in enumerate(mlwh_samples)}

 for roma_sample in roma_samples["data"]:
  merged_sample = [
      roma_sample["pk"],
      roma_sample["fields"]["sample_name"],
      roma_sample["origin"],
      roma_sample["fields"]["creation_date"],
      roma_sample["fields"]["collection_date"]
      ]
  if merged_sample[1] in mlwh_index:
   mlwh_row = mlwh_samples[mlwh_index[merged_sample[1]]]
   merged_sample.extend(mlwh_row)
  samples.append(merged_sample)
 app.app.logger.info("Samples {}".format(len(samples)))
 return (stamp, samples)

#-------------------------------------------------------------------------------
@cache.cached(key_prefix='sample_view_cache')
def generate_sample_view():
 """ Takes a sample list and translates it into a meta object view """
 samples = get_samples()[1]
 events = get_events()
 summary = []
 app.app.logger.info("Called")

 event_map = {
      "malgen_lab":(3,7),
      "sanger_core":(7,8),
      "gbs_core":(8,12),
      "gbs_analysis":(-1,-1),
      "wgs_core":(-1,-1),
      "wgs_analysis":(-1,-1)
 }

 for sample in samples:

  study_id = "-"
  sanger_id = "-"
  if len(sample) >= 11:
   study_id = sample[5]
   sanger_id = sample[10]


  samp = {
      "ds":[],
      "events":[],
      "ids": {
          "sample_id": sample[1],
          "sscape_id": sanger_id
      },
      "project": sample[2],
      "species": guess_species_by_origin(sample[2]),
      "study_id": study_id
  }

  #build events
  for k,v in event_map.items():
   start = None
   end = None
   if v[0] > -1 and len(sample) >= v[0]:
    start = sample[v[0]]
   if v[1] > -1 and len(sample) >= v[1]:
    end = sample[v[1]]
   samp["events"].append(get_event_status(k,start,end))

  #generate sample status
  sample_status = "N/A"
  for e in samp["events"]:
   if e["status"] != "N/A":
    sample_status = e["status"]
   else:
    break

  samp["processing_status"] = sample_status
  summary.append(samp)

 return summary

#-------------------------------------------------------------------------------
@cache.cached(key_prefix='sample_summary_cache')
def generate_sample_summary():
 """ Takes a sample list and translates it into a view """
 samples = generate_sample_view()
 states = ["N/A", "pending", "complete"]
 event_list = get_events()
 events = {e:{"name":e,"states":{s:0 for s in states}} for e in get_events()}
 totals = {e:0 for e in get_events()}

 for sample in samples:
  for event in sample["events"]:
   if event["name"] in events:
    events[event["name"]]["states"][event["status"]] += 1
    totals[event["name"]] += 1

 summary = [event_data for event_name,event_data in events.items()]
 for event_summary in summary:
  event_summary["total"] = totals[event_summary["name"]]

 return summary

#-------------------------------------------------------------------------------
@scheduler.schedule(run_on_start=True, minutes=90)
def rebuild_samples():
 """Method to refresh get_samples cache"""
 app.app.logger.info("rebuild_samples fired")
 cache.delete('sample_cache')
 get_samples()
 cache.delete('sample_view_cache')
 generate_sample_view()
 cache.delete('sample_summary_cache')
 generate_sample_summary()
 return {"status":"rebuilt"}
#-------------------------------------------------------------------------------
def get_samples_page(offset, limit):
 """ get a subset of samples. index = offset * limit """
 page = limit * offset
 get_sample_results = generate_sample_view()
 samples = get_sample_results
 sample_len = len(samples)
 #app.app.logger.info("Samples {} Stamp {}".format(sample_len, get_sample_results[0]))
 if sample_len >= page:
  if page + limit > sample_len:
   limit = sample_len
  app.app.logger.info("Returning {}:{}".format(page, page + limit))
  return samples[page:page + limit],len(samples)

 return ["Max length {}".format(sample_len)]
