""" Module containing S3 bucket helper functions """
import io
import json
import boto3

#-- Module members -------------------------------------------------------------
__s3_session = None

#-- S3 functions ---------------------------------------------------------------
def get_client(access, secret, region="eu-west-2"):
 """ create a boto3 session and client. Returns client and holds on to session """

 global __s3_session
 if __s3_session is None:
  __s3_session = boto3.session.Session()
 return __s3_session.client(
     service_name='s3',
     region_name=region,
     aws_access_key_id=access,
     aws_secret_access_key=secret
 )

#-------------------------------------------------------------------------------
def select_latest_key_from_list(keys, prefix):
 """ returns the latest key for a given prefix. Expects keys to contain YYYYMMDD"""
 target_keys = sorted([k for k in keys if k.startswith(prefix)], reverse=True)
 return  target_keys[0] if len(target_keys) > 0 else None

#-------------------------------------------------------------------------------
def get_object_keys(client, bucket):
 """ get a list of object keys directly from a bucket """
 objs = client.list_objects(Bucket=bucket)
 key_objs = objs["Contents"]
 return [k["Key"] for k in key_objs]

#-------------------------------------------------------------------------------
def select_latest_key_from_bucket(client, bucket, prefix):
 """ Helper method calls select_latest_key_from_list passing in
 get_object_keys as a list of keys """
 return select_latest_key_from_list(get_object_keys(client, bucket), prefix)

#-------------------------------------------------------------------------------
def get_json_from_object(client, bucket, key):
 """ Streams an object from a given bucket and converts it into a Dict"""
 stream = io.BytesIO()
 client.download_fileobj(bucket, key, stream)
 stream.seek(0)
 wrapper = io.TextIOWrapper(stream, encoding='utf-8')
 return json.load(wrapper)

#-------------------------------------------------------------------------------
def get_xsv_from_object(client, bucket, key, seperator):
 """ load a s3 object and convert it into a 2D array (list of lists)"""
 stream = io.BytesIO()
 client.download_fileobj(bucket, key, stream)
 stream.seek(0)
 wrapper = io.TextIOWrapper(stream, encoding='utf-8')
 fields = []
 for line in wrapper:
  fields.append(list(line.split(seperator)))
 return fields

#-------------------------------------------------------------------------------
def get_tsv_from_object(client, bucket, key):
 """ Convert a tsv object into a 2D array """
 return get_xsv_from_object(client, bucket, key, "\t")

#-------------------------------------------------------------------------------
def get_csv_from_object(client, bucket, key):
 """ Convert a csv object into a 2D array """
 return get_xsv_from_object(client, bucket, key, ",")
