"""
Conflux server - Honours the OpenApi contract defined here
https://gitlab.com/malariagen/conflux/api-contract.
Uses the simple datastores to provide content for the endpoints
"""

import sys
import os
from flask import Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from flask_caching import Cache
import connexion

from conflux_server.env_config import get_connection_url, get_sso_host
from conflux_server.scheduler import DecoratedScheduler

#--- module level decorator instances required by other modules
app = None
cache = None
scheduler = None

def get_app(port=0, contract_path="/contract"):
 """
 Factory method for instantiating the conlux server.
 """
 global app
 global cache
 global scheduler

 # Debug mode restarts the server after it has started resulting in the cache
 # firing twice. It would be good to find a way to prevent this.
 is_debug = len(os.getenv('CONFLUX_FLASK_DEBUG', "")) > 0

 use_reloader = False
 oauth2_url = get_sso_host()
 if is_debug:
  use_reloader = True

 app_options = {"swagger_ui":False}

 if is_debug:
  app_options = {
      "swagger_ui_config":{
          "oauth2RedirectUrl":"http://localhost:{}/ui/oauth2-redirect.html".format(port)
      }
  }

 #Setup main objects
 cache = Cache(config={'CACHE_TYPE':'simple'})
 scheduler = DecoratedScheduler()
 app = connexion.FlaskApp(__name__, specification_dir=contract_path, options=app_options)

 #Configure app
 api_options = {"sso": oauth2_url}
 app.add_api('api.yaml', arguments=api_options)
 app.app.config["SQLALCHEMY_DATABASE_URI"] = get_connection_url()

 #Remove deprecation warning
 app.app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
 db_conn = SQLAlchemy(app.app)
 # disabled as currently we are not using a db. Not sure if we ever should
 #Migrate(app.app, db_conn)
 CORS(app.app)
 cache.init_app(app.app)

 scheduler.startup()

 if is_debug:
  return app, db_conn, use_reloader

 return app
